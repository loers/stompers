//! The session module implements the protocol for a STOMP client session.

use std::{cmp::max, fmt::Debug, time::Duration};

use async_std::{
    channel::{self, Receiver, RecvError, Sender},
    future::{timeout, TimeoutError},
    task::{self, JoinHandle},
};

use futures::{
    channel::mpsc::channel, select, Future, FutureExt, Sink, SinkExt, Stream, StreamExt,
};
use url::Url;
use uuid::Uuid;

use crate::{
    frame::{
        connect_frame, disconnect_frame, send_frame, subscribe_frame, Ack, Frame, ParseError,
        COMMAND_RECEIPT,
    },
    prelude::{ack_frame, nack_frame},
};

#[derive(Debug)]
pub enum StompError {
    Connect(String),
    Handshake,
    Heartbeat,
    Url(String),
    Parse(ParseError),
    Other(String),
}

#[derive(Debug)]
pub struct Session {
    session_id: String,
    task: JoinHandle<()>,
    sender: Sender<Option<Frame>>,
    receiver: Receiver<Frame>,
    subscription_id: async_std::sync::Mutex<u128>,
    heartbeat: Option<Duration>,
}

impl Session {
    pub async fn start<
        S: Sink<Frame, Error = E>
            + Stream<Item = Result<Frame, E>>
            + Unpin
            + std::marker::Send
            + 'static,
        E: Debug + Into<StompError> + std::marker::Send,
    >(
        endpoint: &str,
        credentials: Option<(String, String)>,
        heartbeat: Option<(u64, u64)>,
        connect: impl Future<Output = Result<S, StompError>>,
    ) -> Result<Self, StompError> {
        let session_id = Uuid::new_v4().to_string();

        let url = Url::parse(endpoint).map_err(|e| StompError::Url(format!("{}", e)))?;

        let host = url
            .host()
            .map(|h| h.to_string())
            .unwrap_or(endpoint.to_string());

        let mut stream = connect.await?;

        let answer = Session::handshake(&mut stream, host, credentials, heartbeat).await?;
        let heartbeat = Session::get_heartbeat(heartbeat, answer.heart_beat());

        let (write_sender, write_receiver) = channel::unbounded::<Option<Frame>>();
        let (recv_sender, read_receiver) = channel::unbounded::<Frame>();
        let sid = session_id.clone();
        let t = task::spawn(async move {
            let session_id = sid.clone();
            loop {
                let send_frame_or_heartbeat = async {
                    if heartbeat.is_some() {
                        timeout(heartbeat.unwrap(), write_receiver.recv()).await
                    } else {
                        Ok(write_receiver.recv().await)
                    }
                };

                let recv_frame = stream.next();

                enum Action<E> {
                    Recv(Option<Result<Frame, E>>),
                    Send(Result<Result<Option<Frame>, RecvError>, TimeoutError>),
                }

                let action: Action<E> = select!(
                    send = send_frame_or_heartbeat.fuse() => Action::Send(send),
                    recv = recv_frame.fuse() => Action::Recv(recv)
                );

                match action {
                    Action::Recv(recv) => {
                        if let Some(r) = recv {
                            match r.map_err(|e| e.into()) {
                                Ok(frame) => {
                                    if frame.command() == COMMAND_RECEIPT
                                        && (frame.header("receipt-id") == Some(&session_id)
                                            || frame.header("receipt-id") == Some("stop"))
                                    {
                                        info!("Stopped STOMP session.");
                                        return;
                                    }

                                    if frame.command().is_empty() {
                                        // heartbeat
                                        continue;
                                    }

                                    debug!("Received frame {:?}", frame);
                                    recv_sender
                                        .send(frame)
                                        .await
                                        .expect("Could not internally send frame.");
                                }
                                Err(e) => {
                                    error!("{:?}", e);
                                    // todo
                                }
                            }
                        } else {
                            error!("Received None frame");
                            return;
                            // todo
                        }
                    }
                    Action::Send(send) => {
                        if let Ok(frame) = send {
                            match frame {
                                Ok(Some(frame)) => {
                                    debug!("Sending frame {:?}", frame);
                                    stream
                                        .send(frame)
                                        .await
                                        .map_err(|e| e.into())
                                        .expect("Could not send frame");
                                }
                                Ok(None) => {
                                    debug!("Disconnecting...");
                                    let frame = disconnect_frame(session_id.clone());
                                    stream
                                        .send(frame)
                                        .await
                                        .map_err(|e| e.into())
                                        .expect("Could not send disconnect frame");
                                }
                                Err(_e) => {
                                    // todo
                                }
                            }
                        } else {
                            Session::write_heartbeat(&mut stream, heartbeat.unwrap())
                                .await
                                .expect("Could not send frame");
                        }
                    }
                }
            }
        });

        Ok(Session {
            session_id,
            task: t,
            heartbeat,
            sender: write_sender,
            receiver: read_receiver,
            subscription_id: async_std::sync::Mutex::new(0),
        })
    }

    #[cfg(feature = "async_tungstenite")]
    pub async fn start_with_tungstenite(
        url: Url,
        credentials: Option<(String, String)>,
        heartbeat: Option<(u64, u64)>,
    ) -> Result<Self, StompError> {
        use std::sync::Mutex;

        let stream = async {
            let (ws, _) = async_tungstenite::async_std::connect_async(url.clone())
                .await
                .map_err(|e| StompError::Connect(format!("{:?}", e)))?;
            Ok(crate::transport::tungstenite::TungsteniteAdapter { ws: Mutex::new(ws) })
        };
        Session::start(url.as_str(), credentials, heartbeat, stream).await
    }

    pub async fn stop(self) {
        self.sender.send(None).await.expect("Could not stop");
        self.task.await;
    }

    pub async fn send(
        &self,
        destination: &str,
        body: &str,
        content_type: Option<&str>,
    ) -> Result<(), StompError> {
        let frame = send_frame(
            destination.into(),
            body.into(),
            content_type.map(|s| s.into()),
        );

        if let Err(e) = self.sender.send(Some(frame)).await {
            Err(StompError::Other(format!("{:?}", e)))
        } else {
            Ok(())
        }
    }

    pub async fn subscribe(
        &self,
        destination: &str,
        ack: Ack,
    ) -> Result<futures::channel::mpsc::Receiver<Frame>, StompError> {
        let (mut callback, callback_recv) = channel(100);
        let frame = subscribe_frame(
            destination.into(),
            self.sub_id().await.to_string(),
            ack.clone(),
        );
        self.sender
            .send(Some(frame))
            .await
            .map_err(|e| StompError::Other(format!("{:?}", e)))?;
        let destination = destination.to_string();
        let mut recv = self.receiver.clone();

        let ack = ack.clone();
        let ack_sender = self.sender.clone();

        task::spawn(async move {
            while let Some(received_frame) = recv.next().await {
                let dest = received_frame.header("destination");
                if dest.is_none() {
                    continue;
                }
                let dest = dest.unwrap();
                let matches_destination = dest == &destination;
                let matches_queue_destination = dest == &format!("/queue/{}", destination);
                if matches_destination || matches_queue_destination {
                    if ack == Ack::Client && received_frame.header("ack").is_some() {
                        let received_ack = received_frame.header("ack").unwrap();
                        if let Err(e) = ack_sender
                            .send(ack_frame(received_ack.into(), None).into())
                            .await
                        {
                            error!("Could not send ack frame: {:?}", e);
                        }
                    }
                    if let Err(e) = callback.send(received_frame.clone()).await {
                        error!(
                            "Could not internally send received frame {:?}: {:?}",
                            received_frame, e
                        );

                        let received_ack = received_frame.header("ack").unwrap();
                        if let Err(e) = ack_sender
                            .send(nack_frame(received_ack.into(), None).into())
                            .await
                        {
                            error!("Could not send nack frame: {:?}", e);
                        }
                    }
                }
            }
        });
        Ok(callback_recv)
    }

    pub fn heartbeat(&self) -> Option<Duration> {
        self.heartbeat
    }

    async fn handshake<
        S: Sink<Frame, Error = E> + Stream<Item = Result<Frame, E>> + Unpin,
        E: Debug + Into<StompError>,
    >(
        stream: &mut S,
        host: String,
        credentials: Option<(String, String)>,
        heartbeat: Option<(u64, u64)>,
    ) -> Result<Frame, StompError> {
        let frame = connect_frame(host.clone(), credentials, heartbeat);
        debug!("Establish STOMP connection: {:?}", frame);
        if let Err(e) = stream.send(frame).await {
            error!("Could not perform handshake: {:?}", e);
            return Err(e.into());
        }

        match stream.next().await {
            Some(Ok(frame)) => {
                debug!("Connection successful: {:?}", frame);
                Ok(frame)
            }
            Some(Err(e)) => {
                debug!("Could not establish connection: {:?}", e);
                Err(e.into())
            }
            None => Err(StompError::Handshake),
        }
    }

    fn get_heartbeat(
        client_heartbeat: Option<(u64, u64)>,
        server_hearbeat: Option<(u64, u64)>,
    ) -> Option<Duration> {
        client_heartbeat
            .map(|own| server_hearbeat.map(|server| (own, server)))
            .flatten()
            .map(
                |((client_can, _client_wants), (_server_can, server_wants))| {
                    if client_can > 0 && server_wants > 0 {
                        Some(Duration::from_millis(max(client_can, server_wants)))
                    } else {
                        None
                    }
                },
            )
            .flatten()
    }

    async fn write_heartbeat<S: Sink<Frame, Error = E> + Unpin, E: Debug + Into<StompError>>(
        stream: &mut S,
        heartbeat: Duration,
    ) -> Result<(), StompError> {
        if let Ok(Ok(_)) = timeout(heartbeat, stream.send(crate::frame::heartbeat())).await {
            // heartbeat is sent.
        } else {
            stream.close().await.expect("Could not close session.");
            return Err(StompError::Heartbeat);
        }
        Ok(())
    }

    async fn sub_id(&self) -> u128 {
        let mut id = self.subscription_id.lock().await;
        let next_id = id.clone();
        *id = next_id + 1;
        return next_id;
    }
}

#[cfg(test)]
mod tests {

    use std::{
        cell::Cell,
        pin::Pin,
        task::{Context, Poll},
    };

    use async_std::task;
    use futures::TryFutureExt;

    use crate::frame::{connected_frame, parse, send_frame, subscribe_frame, Ack};

    use super::*;

    struct MockStream {
        read_data: Vec<Vec<u8>>,
        read_index: Cell<usize>,
        write_data: Cell<Vec<u8>>,
    }

    impl MockStream {
        pub async fn connect(_host: &str) -> std::io::Result<Self> {
            Ok(MockStream {
                read_data: Default::default(),
                read_index: Cell::new(0),
                write_data: Default::default(),
            })
        }

        pub fn mock_answer(&mut self, frame: Frame) {
            self.read_data.push(frame.as_bytes());
        }
    }

    impl Stream for MockStream {
        type Item = Result<Frame, StompError>;

        fn poll_next(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
            let read_index = self.read_index.take();
            let data = self.read_data.get(read_index);
            if data.is_some() {
                let frame = parse(&String::from_utf8(data.unwrap().clone()).unwrap()).unwrap();
                self.read_index.set(read_index + 1);
                Poll::Ready(Some(Ok(frame)))
            } else {
                self.read_index.set(read_index);
                Poll::Ready(None)
            }
        }
    }

    impl Sink<Frame> for MockStream {
        type Error = StompError;

        fn poll_ready(
            self: Pin<&mut Self>,
            _cx: &mut Context<'_>,
        ) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }

        fn start_send(self: Pin<&mut Self>, item: Frame) -> Result<(), Self::Error> {
            let mut data = self.write_data.take();
            for b in item.as_bytes() {
                data.push(b);
            }
            self.write_data.set(data);
            Ok(())
        }

        fn poll_flush(
            self: Pin<&mut Self>,
            _cx: &mut Context<'_>,
        ) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }

        fn poll_close(
            self: Pin<&mut Self>,
            _cx: &mut Context<'_>,
        ) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
    }

    #[test]
    fn test_handshake() {
        task::block_on(async {
            let mut stream = MockStream::connect("").await.unwrap();

            stream.mock_answer(connected_frame("1.2".into()));

            let received_frame = Session::handshake(
                &mut stream,
                "localhost".into(),
                Some(("user123".into(), "password123".into())),
                Some((100, 200)),
            )
            .await
            .unwrap();

            let write_data = stream.write_data.take();
            let sent_frame = parse(std::str::from_utf8(&write_data).unwrap()).unwrap();

            assert_eq!(received_frame.command(), "CONNECTED");
            assert_eq!(received_frame.header("version"), Some("1.2"));

            assert_eq!(sent_frame.command(), "CONNECT");
            assert_eq!(sent_frame.header("accept-version"), Some("1.0,1.1,1.2"));
            assert_eq!(sent_frame.header("login"), Some("user123"));
            assert_eq!(sent_frame.header("passcode"), Some("password123"));
            assert_eq!(sent_frame.header("heart-beat"), Some("100,200"));
        })
    }

    #[test]
    fn test_get_heartbeat_server_wins() {
        let client_wants = 100;
        let client_can = 200;
        let server_wants = 100;
        let server_can = 200;
        let heartbeat = Session::get_heartbeat(
            Some((client_wants, client_can)),
            Some((server_wants, server_can)),
        );
        assert_eq!(heartbeat, Some(Duration::from_millis(server_can)))
    }

    #[test]
    fn test_get_heartbeat_client_wins() {
        let client_wants = 500;
        let client_can = 200;
        let server_wants = 100;
        let server_can = 200;
        let heartbeat = Session::get_heartbeat(
            Some((client_wants, client_can)),
            Some((server_wants, server_can)),
        );
        assert_eq!(heartbeat, Some(Duration::from_millis(client_wants)))
    }

    #[test]
    fn test_get_heartbeat_server_has_no_heartbeat() {
        let client_wants = 500;
        let client_can = 200;
        let heartbeat = Session::get_heartbeat(Some((client_wants, client_can)), None);
        assert_eq!(heartbeat, None)
    }

    #[test]
    fn test_get_heartbeat_client_has_no_heartbeat() {
        let server_wants = 100;
        let server_can = 200;
        let heartbeat = Session::get_heartbeat(None, Some((server_wants, server_can)));
        assert_eq!(heartbeat, None)
    }

    #[test]
    fn test_session() {
        task::block_on(async {
            let endpoint = "localhost:8090";

            let stream = async {
                let mut stream = MockStream::connect(endpoint)
                    .map_err(|e| StompError::Connect(format!("{:?}", e)))
                    .await
                    .unwrap();
                stream.mock_answer(connected_frame("1.2".into()));
                Ok(stream)
            };

            let session = Session::start(
                endpoint,
                Some(("user123".into(), "password123".into())),
                Some((1, 2)),
                stream,
            )
            .await;

            assert!(session.is_ok());
            let session = session.unwrap();

            // send a message
            let send_result = session
                .sender
                .send(Some(send_frame(
                    "Test".into(),
                    "".into(),
                    Some("text/plain".into()),
                )))
                .await;
            assert!(send_result.is_ok());

            // TODO: assert mockstream.write_data

            // subscribe to destination
            let sub_result = session
                .sender
                .send(Some(subscribe_frame(
                    "/test-destination".into(),
                    session.sub_id().await.to_string(),
                    Ack::Auto,
                )))
                .await;
            assert!(sub_result.is_ok());
            // TODO: assert mockstream.write_data
        })
    }
}
