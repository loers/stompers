//! The frame module implements SOMP Frames.

pub const HEADER_ACCEPT_VERSION: &str = "accept-version";
pub const HEADER_HOST: &str = "host";
pub const HEADER_HEART_BEAT: &str = "heart-beat";

pub const COMMAND_CONNECT: &str = "CONNECT";
pub const COMMAND_CONNECTED: &str = "CONNECTED";
pub const COMMAND_SEND: &str = "SEND";
pub const COMMAND_MESSAGE: &str = "MESSAGE";
pub const COMMAND_ERROR: &str = "ERROR";
pub const COMMAND_SUBSCRIBE: &str = "SUBSCRIBE";
pub const COMMAND_DISCONNECT: &str = "DISCONNECT";
pub const COMMAND_RECEIPT: &str = "RECEIPT";
pub const COMMAND_ACK: &str = "ACK";
pub const COMMAND_NACK: &str = "NACK";

const COMMANDS: [&str; 6] = [
    COMMAND_CONNECT,
    COMMAND_CONNECTED,
    COMMAND_ERROR,
    COMMAND_MESSAGE,
    COMMAND_DISCONNECT,
    COMMAND_RECEIPT,
];

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Frame {
    pub(crate) command: String,
    pub(crate) headers: Vec<(String, String)>,
    pub(crate) body: String,
}

impl Frame {
    fn new(command: &str, headers: Vec<(String, String)>, body: String) -> Self {
        Frame {
            command: command.to_string(),
            headers: headers
                .into_iter()
                .map(|(key, value)| (key, value))
                .collect(),
            body: body.to_string(),
        }
    }

    pub fn command(&self) -> &String {
        &self.command
    }

    pub fn header(&self, name: &str) -> Option<&str> {
        self.headers
            .iter()
            .find(|(k, _)| k == name)
            .map(|(_, v)| v.as_str())
    }

    pub fn heart_beat(&self) -> Option<(u64, u64)> {
        let mut found_value = None;
        for (header, value) in &self.headers {
            if header == HEADER_HEART_BEAT {
                // get the last header if there are multiple same ones
                found_value = Some(value);
            }
        }

        found_value
            .map(|v| {
                v.split(",")
                    .map(|v| v.parse::<u64>().ok())
                    .filter(|o| o.is_some())
                    .map(|o| o.unwrap())
                    .collect::<Vec<u64>>()
            })
            .filter(|v| v.len() == 2)
            .map(|v| (v[0].clone(), v[1].clone()))
    }

    pub fn body(&self) -> &String {
        &self.body
    }

    pub fn as_bytes(&self) -> Vec<u8> {
        self.to_string().into_bytes()
    }

    fn encode(string: &String) -> String {
        string
            .replace("\\", "\\\\")
            .replace("\r", "\\r")
            .replace("\n", "\\n")
            .replace("\u{58}", "\\c")
    }

    fn decode(string: String) -> String {
        string
            .replace("\\r", "\r")
            .replace("\\n", "\n")
            .replace("\\c", "\u{58}")
            .replace("\\\\", "\\")
    }

    pub fn to_string(&self) -> String {
        if self.command.is_empty() {
            // consider this a heartbeat frame
            return "\0".into();
        }
        let headers: String = self
            .headers
            .iter()
            .map(|(k, v)| format!("{}:{}\n", Frame::encode(k), Frame::encode(v)))
            .collect::<Vec<String>>()
            .join("");
        format!(
            "{}\n{}\n{}\0",
            self.command,
            headers,
            Self::encode(&self.body)
        )
    }
}

impl Into<String> for Frame {
    fn into(self) -> String {
        self.to_string()
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Ack {
    Auto,
    Client,
    ClientIndividual,
}

impl Ack {
    fn to_string(&self) -> String {
        match self {
            Ack::Auto => "auto".into(),
            Ack::Client => "client".into(),
            Ack::ClientIndividual => "client-individual".into(),
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum ParseError {
    Partial,
    UnknownCommand,
    MalformedHeader,
}

pub fn parse(frame: &str) -> Result<Frame, ParseError> {
    if frame == "\n" {
        // heartbeat
        return Ok(Frame::new("", vec![], "".into()));
    }

    if !frame.ends_with("\0") {
        return Err(ParseError::Partial);
    }

    let lines: Vec<&str> = frame.lines().collect();
    let command;
    match lines.get(0).cloned() {
        Some(l) if COMMANDS.to_vec().contains(&l) => command = l,
        _ => {
            return Err(ParseError::UnknownCommand);
        }
    }

    let mut headers = vec![];

    let mut header_end = 1;
    for line in &lines[1..] {
        header_end += 1;
        let line = *line;
        if line == "" {
            break;
        }
        let colon_index;

        match line.find(":") {
            Some(a) => colon_index = a,
            None => {
                return Err(ParseError::MalformedHeader);
            }
        };
        let (key, value) = line.split_at(colon_index);
        headers.push((key.to_string(), Frame::decode(value[1..].to_string())));
    }

    let body = Frame::decode(lines[header_end..].concat().replace("\0", ""));

    Ok(Frame {
        command: command.to_string(),
        headers,
        body,
    })
}

pub fn heartbeat() -> Frame {
    Frame {
        command: "".into(),
        headers: Vec::new(),
        body: "".into(),
    }
}

/// Creates a CONNECT frame.
/// ```
/// use stompers::frame::connect_frame;
///
/// let frame = connect_frame("localhost".into(), Some(("test".into(), "pwd".into())), Some((1000, 2000)));
///
/// assert_eq!(frame.to_string(), "CONNECT\naccept-version:1.0,1.1,1.2\nhost:localhost\nlogin:test\npasscode:pwd\nheart-beat:1000,2000\n\n\0")
/// ```
pub fn connect_frame(
    hostname: String,
    credentials: Option<(String, String)>,
    heartbeat: Option<(u64, u64)>,
) -> Frame {
    let mut headers = vec![
        ("accept-version".into(), "1.0,1.1,1.2".into()),
        ("host".into(), hostname),
    ];
    if credentials.is_some() {
        let credentials = credentials.unwrap();
        headers.push(("login".into(), credentials.0));
        headers.push(("passcode".into(), credentials.1));
    }
    if heartbeat.is_some() {
        let heartbeat = heartbeat.unwrap();
        headers.push((
            "heart-beat".into(),
            format!("{},{}", heartbeat.0, heartbeat.1),
        ));
    }
    Frame::new(COMMAND_CONNECT, headers, "".into())
}

/// Creates a CONNECTED frame.
/// ```
/// use stompers::frame::connected_frame;
///
/// let frame = connected_frame("1.0,1.1".into());
///
/// assert_eq!(frame.to_string(), "CONNECTED\nversion:1.0,1.1\n\n\0")
/// ```
pub fn connected_frame(versions: String) -> Frame {
    Frame::new(
        COMMAND_CONNECTED,
        vec![("version".into(), versions)],
        "".into(),
    )
}

/// Creates a DISCONNECT frame.
/// ```
/// use stompers::frame::disconnect_frame;
///
/// let frame = disconnect_frame("0".into());
///
/// assert_eq!(frame.to_string(), "DISCONNECT\nreceipt:0\n\n\0")
/// ```
pub fn disconnect_frame(receipt: String) -> Frame {
    Frame::new(
        COMMAND_DISCONNECT,
        vec![("receipt".into(), receipt)],
        "".into(),
    )
}

/// Creates a SEND frame.
///
/// If the content type argument is None the `content-type` header will be set to `text/plain`.
///
/// ```
/// use stompers::frame::send_frame;
///
/// let frame = send_frame("/dest".into(), "Message".into(), Some("text/plain".into()));
///
/// assert_eq!(frame.to_string(), "SEND\ndestination:/dest\ncontent-type:text/plain\ncontent-length:7\n\nMessage\0")
/// ```
pub fn send_frame(destination: String, body: String, content_type: Option<String>) -> Frame {
    Frame::new(
        COMMAND_SEND,
        vec![
            ("destination".into(), destination),
            (
                "content-type".into(),
                content_type.unwrap_or("text/plain".into()),
            ),
            (
                "content-length".into(),
                Frame::encode(&body).len().to_string(),
            ),
        ],
        body,
    )
}

/// Creates a SUBSCRIBE frame.
///
/// ```
/// use stompers::frame::{subscribe_frame, Ack};
///
/// let frame = subscribe_frame("/dest".into(), "0".into(), Ack::Auto);
///
/// assert_eq!(frame.to_string(), "SUBSCRIBE\ndestination:/dest\nid:0\nack:auto\n\n\0")
/// ```
pub fn subscribe_frame(destination: String, id: String, ack: Ack) -> Frame {
    Frame::new(
        COMMAND_SUBSCRIBE,
        vec![
            ("destination".into(), destination),
            ("id".into(), id),
            ("ack".into(), ack.to_string()),
        ],
        "".into(),
    )
}

/// Creates a RECEIPT frame.
///
/// ```
/// use stompers::frame::receipt_frame;
///
/// let frame = receipt_frame("77".into());
///
/// assert_eq!(frame.to_string(), "RECEIPT\nreceipt-id:77\n\n\0")
/// ```
pub fn receipt_frame(receipt_id: &str) -> Frame {
    Frame::new(
        COMMAND_RECEIPT,
        vec![("receipt-id".into(), receipt_id.into())],
        "".into(),
    )
}

/// Creates an ACK frame.
///
/// ```
/// use stompers::frame::ack_frame;
///
/// let frame = ack_frame("12345".into(), Some("tx1".into()));
///
///
/// assert_eq!(frame.to_string(), "ACK\nid:12345\ntransaction:tx1\n\n\0")
/// ```
pub fn ack_frame(id: String, transaction: Option<String>) -> Frame {
    let mut headers = vec![("id".into(), id)];
    if transaction.is_some() {
        headers.push(("transaction".into(), transaction.unwrap()));
    }
    Frame {
        command: COMMAND_ACK.into(),
        headers,
        body: "".into(),
    }
}

/// Creates an NACK frame.
///
/// ```
/// use stompers::frame::nack_frame;
///
/// let frame = nack_frame("12345".into(), Some("tx1".into()));
///
///
/// assert_eq!(frame.to_string(), "NACK\nid:12345\ntransaction:tx1\n\n\0")
/// ```
pub fn nack_frame(id: String, transaction: Option<String>) -> Frame {
    let mut headers = vec![("id".into(), id)];
    if transaction.is_some() {
        headers.push(("transaction".into(), transaction.unwrap()));
    }
    Frame {
        command: COMMAND_NACK.into(),
        headers,
        body: "".into(),
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use crate::frame::Frame;

    #[test]
    fn parse_command_connect() {
        let s = "CONNECT\n\n\0".to_string();
        assert_eq!(
            parse(&s).ok(),
            Some(Frame {
                command: "CONNECT".to_string(),
                headers: vec![],
                body: "".to_string(),
            })
        );
    }

    #[test]
    fn parse_command_connect_1header() {
        let s = "CONNECT\nh:1\n\n\0".to_string();
        assert_eq!(
            parse(&s).ok(),
            Some(Frame {
                command: "CONNECT".to_string(),
                headers: vec![("h".to_string(), "1".to_string())],
                body: "".to_string(),
            })
        );
    }

    #[test]
    fn parse_command_connect_2headers() {
        let s = "CONNECT\nh:1\nk:2\n\n\0".to_string();
        assert_eq!(
            parse(&s).ok(),
            Some(Frame {
                command: "CONNECT".to_string(),
                headers: vec![
                    ("h".to_string(), "1".to_string()),
                    ("k".to_string(), "2".to_string())
                ],
                body: "".to_string(),
            })
        );
    }

    #[test]
    fn parse_command_connect_body() {
        let s = "CONNECT\nh:1\nk:2\n\nThis is a body\n\0".to_string();
        assert_eq!(
            parse(&s).ok(),
            Some(Frame {
                command: "CONNECT".to_string(),
                headers: vec![
                    ("h".to_string(), "1".to_string()),
                    ("k".to_string(), "2".to_string())
                ],
                body: "This is a body".to_string(),
            })
        );
    }

    #[test]
    fn parse_wrong_command() {
        let s = "BLA\nh1:123\n\n\0".to_string();
        assert_eq!(parse(&s).unwrap_err(), ParseError::UnknownCommand);
    }

    #[test]
    fn parse_command_connect_1header_invalid() {
        let s = "CONNECT\nh1\n\n\0".to_string();
        assert_eq!(parse(&s).unwrap_err(), ParseError::MalformedHeader);
    }

    #[test]
    fn parse_command_connect_no_null_byte() {
        let s = "CONNECT\nh:1\n\nABC\n".to_string();
        assert_eq!(parse(&s).unwrap_err(), ParseError::Partial);
    }

    #[test]
    fn parse_command_empty() {
        let s = "".to_string();
        assert_eq!(parse(&s).unwrap_err(), ParseError::Partial);
    }

    #[test]
    fn test_escaping() {
        assert_eq!(Frame::decode(Frame::encode(&":".into())), ":");
        assert_eq!(Frame::decode(Frame::encode(&"\n".into())), "\n");
        assert_eq!(Frame::decode(Frame::encode(&"\r".into())), "\r");
        assert_eq!(Frame::decode(Frame::encode(&"\\".into())), "\\");
    }
}
