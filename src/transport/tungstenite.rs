use std::sync::Mutex;

use async_native_tls::TlsStream;
use async_std::net::TcpStream;
use async_tungstenite::{tungstenite::Message, WebSocketStream};
use futures::{Sink, SinkExt, Stream, StreamExt};

use crate::prelude::*;

pub(crate) struct TungsteniteAdapter {
    pub(crate) ws:
        Mutex<WebSocketStream<async_tungstenite::stream::Stream<TcpStream, TlsStream<TcpStream>>>>,
}

impl Sink<Frame> for TungsteniteAdapter {
    type Error = StompError;

    fn poll_ready(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), Self::Error>> {
        let mut ws = self.ws.lock().unwrap();
        ws.poll_ready_unpin(cx)
            .map_err(|e| StompError::Other(format!("{:?}", e)))
    }

    fn start_send(self: std::pin::Pin<&mut Self>, item: Frame) -> Result<(), Self::Error> {
        let mut ws = self.ws.lock().unwrap();
        ws.start_send_unpin(Message::Text(item.to_string()))
            .map_err(|e| StompError::Other(format!("{:?}", e)))
    }

    fn poll_flush(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), Self::Error>> {
        let mut ws = self.ws.lock().unwrap();
        ws.poll_flush_unpin(cx)
            .map_err(|e| StompError::Other(format!("{:?}", e)))
    }

    fn poll_close(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), Self::Error>> {
        let mut ws = self.ws.lock().unwrap();
        ws.poll_close_unpin(cx)
            .map_err(|e| StompError::Other(format!("{:?}", e)))
    }
}

impl Stream for TungsteniteAdapter {
    type Item = Result<Frame, StompError>;

    fn poll_next(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<Self::Item>> {
        let mut ws = self.ws.lock().unwrap();
        ws.poll_next_unpin(cx).map(|o| match o {
            Some(r) => {
                return match r {
                    Ok(Message::Text(t)) => {
                        // t can be a heartbeat frame: "\\n"
                        match parse(&t) {
                            Ok(f) => Some(Ok(f)),
                            Err(e) => Some(Err(StompError::Parse(e))),
                        }
                    }
                    Ok(Message::Close(_)) => Some(Ok(receipt_frame("stop"))),
                    Ok(m) => {
                        warn!("Received unhandled message {:?}", m);
                        None
                    }
                    Err(e) => Some(Err(StompError::Other(format!("{:?}", e)))),
                };
            }
            None => None,
        })
    }
}
