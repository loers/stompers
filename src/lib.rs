#![deny(warnings)]

//! # stompers
//! stompers is a STOMP 1.2 protocol implementation.
//!
//! Features:
//! - Use different transport protocols (async_tungstenite, TODO: udp sockets)
//! - TODO: Synchronous and asynchronous implementation
//!
//! ## Example
//! ```toml
//! # Include stompers in your Cargo.toml with the async_tungstenite for transport.
//! stompers = { version = "*", features = [ "async_tungstenite" ] }
//! ```
//!
//! Asynchronous usage:
//! ```rust,no_run
//! use futures::StreamExt;
//! use stompers::prelude::*;
//! use url::Url;
//!
//! async_std::task::block_on(async {
//!     let session = Session::start_with_tungstenite(
//!         Url::parse("ws://localhost:61614").unwrap(),
//!         Some(("user".into(), "1234".into())),
//!         Some((1000, 1000))
//!     ).await
//!     .expect("Could not connect");
//!
//!     let mut recv = session.subscribe("/foo/bar", Ack::Auto)
//!         .await
//!         .expect("Subscribe failed");
//!     while let Some(frame) = recv.next().await {
//!         println!("{:?}", frame);
//!     }
//!
//!     session.send("/foo/bar", "This is a message.", Some("text/plain"))
//!         .await
//!         .expect("Sending message failed");
//! });
//! ```
//! TODO Synchronous usage:
//! ```rust,ignore
//! use stompers::prelude::*;
//! use url::Url;
//!
//! let session = Session::open(
//!     Url::parse("ws://localhost:61614").unwrap(),
//!     Some(("user".into(), "1234".into())),
//!     Some((1000, 1000))
//! )
//! .expect("Could not connect");
//!
//! let mut recv = session.subscribe("/foo/bar", Ack::Auto).expect("Subscribe failed");
//! std::thread::spawn(move || {
//!     while let Some(frame) = recv.next().await {
//!         println!("{:?}", frame);
//!     }   
//! });
//!
//! session.send("/foo/bar", "This is a message.", Some("text/plain")).expect("Sending message failed");
//! ```

#[macro_use]
extern crate log;

pub mod client;
pub mod frame;
pub mod server;

/// The transport module contains possible adapters for message transporting.
pub mod transport {
    #[cfg(feature = "async_tungstenite")]
    pub mod tungstenite;
}

pub mod prelude {
    pub use crate::client::*;
    pub use crate::frame::*;
}
