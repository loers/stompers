#[macro_use]
extern crate pest_derive;
extern crate from_pest;
#[macro_use]
extern crate pest_ast;
extern crate pest;

use std::net::{SocketAddr, TcpStream};
use std::sync::{Arc, Mutex, MutexGuard, PoisonError};
use std::time::Duration;

use tungstenite::{connect, Message};
use url::Url;
use std::io::{Write, Read};

use stompers::frame::Frame;


use pest::Parser;

mod stomp {
#[derive(Parser)]
#[grammar = "stomp.pest"]
pub struct StompParser;
}

mod ast {
    use super::stomp::Rule;
    use pest::Span;

    fn span_into_str(span: Span) -> &str {
        span.as_str()
    }

    #[derive(Debug, FromPest)]
    #[pest_ast(rule(Rule::header))]
    pub struct Header {
        #[pest_ast(inner(with(span_into_str), with(str::parse), with(Result::unwrap)))]
        pub key: String,
        #[pest_ast(inner(with(span_into_str), with(str::parse), with(Result::unwrap)))]
        pub value: String,
    }

    #[derive(Debug, FromPest)]
    #[pest_ast(rule(Rule::frame))]
    pub struct Frame {
        #[pest_ast(inner(with(span_into_str), with(str::parse), with(Result::unwrap)))]
        pub command: String,
        pub header: Vec<Header>,
        #[pest_ast(inner(with(span_into_str), with(str::parse), with(Result::unwrap)))]
        pub body: String,
    }
}

// fn main() -> std::io::Result<()> {
//     let mut stream = TcpStream::connect("192.168.0.61:61613")?;
//     let connectFrame = Frame::new("CONNECT", vec![("accept-version", "1.2"), ("hostname", "192.168.0.61")], "");
//     stream.write(connectFrame.to_string().as_bytes())?;

//     let f = Frame::read(&mut stream, Some(1000))?;

//     println!("{:?}", f);



//     let sendFrame = Frame::new("SEND", vec![("destination", "/test")], "test");
//     println!("{:?}", sendFrame.to_string());
//     stream.write(sendFrame.to_string().as_bytes())?;

//     let f = Frame::read(&mut stream, Some(1000))?;
//     println!("{:?}", f);

//     Ok(())
// }


fn main(){
    use crate::from_pest::FromPest;
    // let successful_parse = stomp::StompParser::parse(stomp::Rule::frame, "CONNECT\na:b\n\nsome\nbody\0");
    // println!("{:?}", successful_parse);

    let successful_parse = stomp::StompParser::parse(stomp::Rule::frame, "CONNECT\na:b\n\nsome\nbody\0");
    println!("syntax tree: {:?}", successful_parse);

    let mut parse_tree = stomp::StompParser::parse(stomp::Rule::frame, "CONNECT\na:b\n\nbody\0").unwrap();
    let syntax_tree = ast::Frame::from_pest(&mut parse_tree).expect("infallible");
    println!("ast tree: {:?}", syntax_tree);
}