use std::{
    process::Command,
    sync::{Mutex, Once},
    time::Duration,
};

use async_native_tls::TlsStream;
use async_std::{
    future::timeout,
    net::TcpStream,
    task::{self, block_on, JoinHandle},
};
use async_tungstenite::{tungstenite::Message, WebSocketStream};

use futures::{Sink, SinkExt, Stream, StreamExt};
use log::{error, info, warn};
use once_cell::sync::Lazy;
use stompers::{
    client::{Session, StompError},
    frame::{parse, receipt_frame, Ack, Frame},
};
use url::Url;

static SEND_HISTORY: Lazy<Mutex<Vec<Frame>>> = Lazy::new(|| Mutex::default());
static RECV_HISTORY: Lazy<Mutex<Vec<Frame>>> = Lazy::new(|| Mutex::default());

struct TungsteniteAdapter {
    ws: Mutex<WebSocketStream<async_tungstenite::stream::Stream<TcpStream, TlsStream<TcpStream>>>>,
}

impl Sink<Frame> for TungsteniteAdapter {
    type Error = StompError;

    fn poll_ready(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), Self::Error>> {
        let mut ws = self.ws.lock().unwrap();
        ws.poll_ready_unpin(cx)
            .map_err(|e| StompError::Other(format!("{:?}", e)))
    }

    fn start_send(self: std::pin::Pin<&mut Self>, item: Frame) -> Result<(), Self::Error> {
        SEND_HISTORY.lock().unwrap().push(item.clone());

        let mut ws = self.ws.lock().unwrap();
        ws.start_send_unpin(Message::Text(item.to_string()))
            .map_err(|e| StompError::Other(format!("{:?}", e)))
    }

    fn poll_flush(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), Self::Error>> {
        let mut ws = self.ws.lock().unwrap();
        ws.poll_flush_unpin(cx)
            .map_err(|e| StompError::Other(format!("{:?}", e)))
    }

    fn poll_close(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), Self::Error>> {
        let mut ws = self.ws.lock().unwrap();
        ws.poll_close_unpin(cx)
            .map_err(|e| StompError::Other(format!("{:?}", e)))
    }
}

impl Stream for TungsteniteAdapter {
    type Item = Result<Frame, StompError>;

    fn poll_next(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<Self::Item>> {
        let mut ws = self.ws.lock().unwrap();
        ws.poll_next_unpin(cx).map(|o| match o {
            Some(r) => {
                return match r {
                    Ok(Message::Text(t)) => {
                        // t can be a heartbeat frame: "\\n"
                        match parse(&t) {
                            Ok(f) => {
                                RECV_HISTORY.lock().unwrap().push(f.clone());
                                Some(Ok(f))
                            }
                            Err(e) => Some(Err(StompError::Parse(e))),
                        }
                    }
                    Ok(Message::Close(_)) => Some(Ok(receipt_frame("stop"))),
                    Ok(m) => {
                        warn!("Received unhandled message {:?}", m);
                        None
                    }
                    Err(e) => Some(Err(StompError::Other(format!("{:?}", e)))),
                };
            }
            None => None,
        })
    }
}

static INIT: Once = Once::new();

fn setup() {
    println!("");
    INIT.call_once(|| {
        env_logger::builder()
            .filter(Some("stompers"), log::LevelFilter::Info)
            .filter(Some("tungstenite_it"), log::LevelFilter::Info)
            .filter(None, log::LevelFilter::Warn)
            .init();

        Command::new("docker")
            .args(&["stop", "test-activemq"])
            .output()
            .unwrap();
        std::thread::sleep(Duration::from_millis(1000));
        let o = Command::new("docker")
            .args(&[
                "run",
                "-d",
                "--rm",
                "--name",
                "test-activemq",
                "-p",
                "61616:61616",
                "-p",
                "61614:61614",
                "-p",
                "8161:8161",
                "rmohr/activemq",
            ])
            .output()
            .unwrap();
        if o.stderr.len() > 0 {
            warn!(
                "Setting up ActiveMQ in docker returned with error output: {:?}",
                std::str::from_utf8(&o.stderr).unwrap()
            );
        }

        print!("Connecting to ActiveMQ");
        let mut tries = 0;
        while tries < 10 {
            std::thread::sleep(Duration::from_millis(1000));
            print!(".");
            if let Ok(response) = ureq::get("http://127.0.0.1:8161/").call() {
                if response.status() == 200 {
                    break;
                }
            }
            tries += 1;
        }
        println!("");

        if tries == 10 {
            error!("Starting ActiveMQ timed out.");
            assert!(false, "Starting ActiveMQ timed out.");
        }
    });

    SEND_HISTORY.lock().unwrap().clear();
    RECV_HISTORY.lock().unwrap().clear();
}

fn run_test<T>(test: T)
where
    T: FnOnce() -> () + std::panic::UnwindSafe,
{
    setup();
    let result = std::panic::catch_unwind(test);
    assert!(result.is_ok());
}

async fn start_session(
    credentials: Option<(&str, &str)>,
    heartbeat: Option<(u64, u64)>,
    ack: Ack,
    collect: usize,
    destination: &str,
) -> (Session, JoinHandle<Vec<Frame>>) {
    let endpoint = "ws://localhost:61614";
    let stream = async {
        let (ws, _) = async_tungstenite::async_std::connect_async(Url::parse(endpoint).unwrap())
            .await
            .map_err(|e| StompError::Connect(format!("{:?}", e)))?;
        Ok(TungsteniteAdapter { ws: Mutex::new(ws) })
    };

    let session = Session::start(
        endpoint,
        credentials.map(|(l, p)| (l.into(), p.into())),
        heartbeat,
        stream,
    )
    .await;
    assert!(session.is_ok(), "{:?}", session);
    let session = session.unwrap();

    let mut recv = session.subscribe(destination, ack).await.unwrap();
    let task: JoinHandle<Vec<Frame>> = task::spawn(async move {
        let mut frames = Vec::new();
        while frames.len() < collect {
            if let Some(frame) = timeout(Duration::from_millis(60000), recv.next())
                .await
                .expect("Integration test receive timed out")
            {
                info!("Recveived frame: {:?}", frame,);
                frames.push(frame)
            } else {
                warn!("Received None frame.");
                break;
            }
        }
        return frames;
    });
    return (session, task);
}

#[cfg(test)]
mod its {

    use async_std::task::sleep;

    use super::*;

    const LOREM_IPSUM: &str = {
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, \
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo \
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore \
        eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa \
        qui officia deserunt mollit anim id est laborum."
    };

    const JSON: &str = {
        r#"{
        "_id": "60df4a0afeeeac34d8730b88",
        "index": 0,
        "guid": "23fef0c8-d652-40c5-88d0-1958f13975a9",
        "isActive": true,
        "balance": "$1,791.83",
        "picture": "http://placehold.it/32x32",
        "age": 26,
        "eyeColor": "blue",
        "name": "Jane Conner",
        "gender": "female",
        "company": "KONNECT",
        "email": "janeconner@konnect.com",
        "phone": "+1 (916) 433-3710",
        "address": "989 Fulton Street, Glendale, Colorado, 3073",
        "about": "Minim aute officia cupidatat consequat sit deserunt eiusmod.",
        "registered": "2019-03-23T02:41:35 -01:00",
        "latitude": -78.021619,
        "longitude": -12.823615,
        "tags": [
            "eiusmod",
            "veniam",
            "labore",
            "eu",
            "nisi",
            "Lorem",
            "incididunt"
        ],
        "friends": [
            {
            "id": 0,
            "name": "Alison Mcconnell"
            },
            {
            "id": 1,
            "name": "Sherry Huff"
            },
            {
            "id": 2,
            "name": "Rodgers Witt"
            }
        ],
        "greeting": "Hello, Jane Conner! You have 5 unread messages.",
        "favoriteFruit": "strawberry"
    }"#
    };

    #[test]
    fn test_auto_ack() {
        run_test(|| {
            block_on(async {
                info!("Starting test");
                let (session, frames) = start_session(
                    None,
                    Some((1000, 1000)),
                    Ack::Auto,
                    3,
                    "test_heartbeat_auto_ack",
                )
                .await;

                assert_eq!(session.heartbeat(), Some(Duration::from_millis(1000)));

                sleep(Duration::from_millis(4000)).await;

                assert!(session
                    .send("test_heartbeat_auto_ack", "Message 1", None)
                    .await
                    .is_ok());
                sleep(Duration::from_millis(500)).await;
                assert!(session
                    .send("test_heartbeat_auto_ack", "Message 2", None)
                    .await
                    .is_ok());
                sleep(Duration::from_millis(2000)).await;
                assert!(session
                    .send("test_heartbeat_auto_ack", "Message 3", None)
                    .await
                    .is_ok());

                let frames = frames.await;
                session.stop().await;

                assert_eq!(frames.len(), 3);

                let sent = SEND_HISTORY.lock().unwrap().clone();
                let received = RECV_HISTORY.lock().unwrap().clone();

                assert!(sent.len() == 8 || sent.len() == 9);
                if sent.len() == 8 {
                    assert_eq!(sent[0].command(), "CONNECT");
                    assert_eq!(sent[1].command(), "SUBSCRIBE");
                    assert_eq!(sent[2].command(), "");
                    assert_eq!(sent[3].command(), "SEND");
                    assert_eq!(sent[4].command(), "SEND");
                    assert_eq!(sent[5].command(), "");
                    assert_eq!(sent[6].command(), "SEND");
                    assert_eq!(sent[7].command(), "DISCONNECT");
                } else {
                    assert_eq!(sent[0].command(), "CONNECT");
                    assert_eq!(sent[1].command(), "SUBSCRIBE");
                    assert_eq!(sent[2].command(), "");
                    assert_eq!(sent[3].command(), "");
                    assert_eq!(sent[4].command(), "SEND");
                    assert_eq!(sent[5].command(), "SEND");
                    assert_eq!(sent[6].command(), "");
                    assert_eq!(sent[7].command(), "SEND");
                    assert_eq!(sent[8].command(), "DISCONNECT");
                }

                assert!(received.len() == 7 || received.len() == 8);
                if received.len() == 7 {
                    assert_eq!(received[0].command(), "CONNECTED");
                    assert_eq!(received[1].command(), "");
                    assert_eq!(received[2].command(), "");
                    assert_eq!(received[3].command(), "MESSAGE");
                    assert_eq!(received[4].command(), "MESSAGE");
                    assert_eq!(received[5].command(), "MESSAGE");
                    assert_eq!(received[6].command(), "RECEIPT");
                } else {
                    assert_eq!(received[0].command(), "CONNECTED");
                    assert_eq!(received[1].command(), "");
                    assert_eq!(received[2].command(), "");
                    assert_eq!(received[3].command(), "");
                    assert_eq!(received[4].command(), "MESSAGE");
                    assert_eq!(received[5].command(), "MESSAGE");
                    assert_eq!(received[6].command(), "MESSAGE");
                    assert_eq!(received[7].command(), "RECEIPT");
                }
            });
        })
    }

    #[test]
    fn test_no_heartbeat_auto_ack() {
        run_test(|| {
            block_on(async {
                info!("Starting test");
                let (session, frames) =
                    start_session(None, None, Ack::Auto, 3, "test_no_heartbeat_auto_ack").await;

                assert!(session
                    .send("test_no_heartbeat_auto_ack", "Message 1", None)
                    .await
                    .is_ok());
                assert!(session
                    .send("test_no_heartbeat_auto_ack", LOREM_IPSUM, None)
                    .await
                    .is_ok());
                assert!(session
                    .send("test_no_heartbeat_auto_ack", JSON, None)
                    .await
                    .is_ok());

                let frames = frames.await;
                session.stop().await;

                assert_eq!(frames.len(), 3);

                assert_eq!(frames[0].command(), "MESSAGE");
                assert_eq!(frames[0].header("subscription"), Some("0"));
                assert_eq!(
                    frames[0].header("destination"),
                    Some("/queue/test_no_heartbeat_auto_ack")
                );
                assert_eq!(frames[0].body(), "Message 1");

                assert_eq!(frames[1].body(), LOREM_IPSUM);
                assert_eq!(frames[2].body(), JSON);
            });
        })
    }

    #[test]
    fn test_client_ack() {
        run_test(|| {
            block_on(async {
                info!("Starting test");
                let (session, frames) =
                    start_session(None, Some((1000, 1000)), Ack::Client, 2, "test_client_ack")
                        .await;

                assert_eq!(session.heartbeat(), Some(Duration::from_millis(1000)));

                assert!(session
                    .send("test_client_ack", "Message 1", None)
                    .await
                    .is_ok());

                sleep(Duration::from_millis(500)).await;

                assert!(session
                    .send("test_client_ack", "Message 2", None)
                    .await
                    .is_ok());

                let frames = frames.await;
                session.stop().await;
                assert_eq!(frames.len(), 2);

                let sent = SEND_HISTORY.lock().unwrap().clone();
                let received = RECV_HISTORY.lock().unwrap().clone();

                let sent: Vec<String> = sent.iter().map(|f| f.command().clone()).collect();
                let received: Vec<String> = received.iter().map(|f| f.command().clone()).collect();
                assert_eq!(
                    sent,
                    vec![
                        "CONNECT",
                        "SUBSCRIBE",
                        "SEND",
                        "ACK",
                        "SEND",
                        "ACK",
                        "DISCONNECT"
                    ]
                );
                assert_eq!(received, vec!["CONNECTED", "MESSAGE", "MESSAGE", "RECEIPT"]);
            });
        })
    }
}
